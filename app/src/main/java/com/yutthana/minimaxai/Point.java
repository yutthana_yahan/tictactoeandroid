package com.yutthana.minimaxai;

/**
 * Created by Yutthana on 12/2/15 AD.
 */
class Point {

    int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }
}
