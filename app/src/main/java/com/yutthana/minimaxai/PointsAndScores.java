package com.yutthana.minimaxai;

/**
 * Created by Yutthana on 12/2/15 AD.
 */
class PointAndScore {

    int score;
    Point point;

    PointAndScore(int score, Point point) {
        this.score = score;
        this.point = point;
    }
}