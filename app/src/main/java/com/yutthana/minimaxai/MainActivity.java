package com.yutthana.minimaxai;

import android.graphics.YuvImage;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Board board;

    TextView status;
    Button btn_restart;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.status = (TextView)findViewById(R.id.status);

        // DECLARE AND SET ON CLICK
        this.btn_restart = (Button) findViewById(R.id.btn_restart);
        this.btn0 = (Button) findViewById(R.id.btn0);
        this.btn1 = (Button) findViewById(R.id.btn1);
        this.btn2 = (Button) findViewById(R.id.btn2);
        this.btn3 = (Button) findViewById(R.id.btn3);
        this.btn4 = (Button) findViewById(R.id.btn4);
        this.btn5 = (Button) findViewById(R.id.btn5);
        this.btn6 = (Button) findViewById(R.id.btn6);
        this.btn7 = (Button) findViewById(R.id.btn7);
        this.btn8 = (Button) findViewById(R.id.btn8);

        this.btn_restart.setOnClickListener(this);
        this.btn0.setOnClickListener(this);
        this.btn1.setOnClickListener(this);
        this.btn2.setOnClickListener(this);
        this.btn3.setOnClickListener(this);
        this.btn4.setOnClickListener(this);
        this.btn5.setOnClickListener(this);
        this.btn6.setOnClickListener(this);
        this.btn7.setOnClickListener(this);
        this.btn8.setOnClickListener(this);
        this.btn8.setOnClickListener(this);

        // AI
        StartNewGame();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_restart) {
            // RESTART GAME
            StartNewGame();
        } else {

            int x=0;
            int y=0;

            switch (v.getId()) {

                case R.id.btn0: x=0;y=0;
                    break;
                case R.id.btn1: x=0;y=1;
                    break;
                case R.id.btn2: x=0;y=2;
                    break;
                case R.id.btn3: x=1;y=0;
                    break;
                case R.id.btn4: x=1;y=1;
                    break;
                case R.id.btn5: x=1;y=2;
                    break;
                case R.id.btn6: x=2;y=0;
                    break;
                case R.id.btn7: x=2;y=1;
                    break;
                case R.id.btn8: x=2;y=2;
                    break;
                default:
                    break;

            }
            Point userMove = new Point(x,y);

            buttonTapped(v.getId(),"X"); // Set X tapped at button text GUI
            board.placeAMove(userMove,1); //
            setDisableButtonById(v.getId()); // Disable button

            // AI
            if(this.board.isGameOver()){
                // Game is Over
                if (this.board.XWin()) {
                    this.status.setText("You Win");
                } else if (this.board.OWin()) {
                    this.status.setText("You Lost");
                } else {
                    this.status.setText("Draw!");
                }
                setdisableButtonAll();

            }else{
                board.minimax(0,2);
                board.placeAMove(board.AIMove,2);
                int id = findButtonIdFromPosition(board.AIMove.x,board.AIMove.y);
                buttonTapped(id,"O");// Set O at GUI
                setDisableButtonById(id);

                // Check Winner again
                if(this.board.isGameOver()){
                    // Game is Over
                    if (this.board.XWin()) {
                        this.status.setText("You Win");
                    } else if (this.board.OWin()) {
                        this.status.setText("You Lost");
                    } else {
                        this.status.setText("Draw!");
                    }
                    setdisableButtonAll();
                }
            }

            // End of AI
        }
    }

    public void StartNewGame(){
        this.board = null;
        this.board = new Board();
        setEnableButtonAll();
        setGUIDefault();


    }

    public void setDisableButtonById(int id){
        Button b = (Button)findViewById(id);
        b.setEnabled(false);
    }

    public void setEnableButtonAll(){
        this.btn0.setEnabled(true);
        this.btn1.setEnabled(true);
        this.btn2.setEnabled(true);
        this.btn3.setEnabled(true);
        this.btn4.setEnabled(true);
        this.btn5.setEnabled(true);
        this.btn6.setEnabled(true);
        this.btn7.setEnabled(true);
        this.btn8.setEnabled(true);
    }

    public void setdisableButtonAll(){
        this.btn0.setEnabled(false);
        this.btn1.setEnabled(false);
        this.btn2.setEnabled(false);
        this.btn3.setEnabled(false);
        this.btn4.setEnabled(false);
        this.btn5.setEnabled(false);
        this.btn6.setEnabled(false);
        this.btn7.setEnabled(false);
        this.btn8.setEnabled(false);
    }

    public void buttonTapped(int id,String c){
        Button b = (Button)findViewById(id);
        b.setText(c);
    }

    public int findButtonIdFromPosition(int x, int y){
        if (x == 0 ){
            if (y == 0 ){
                return btn0.getId();
            }
            if (y == 1) {
                return btn1.getId();
            }
            if (y == 2){
                return btn2.getId();
            }
        }
        if (x == 1) {
            if (y == 0 ){
                return btn3.getId();
            }
            if (y == 1) {
                return btn4.getId();
            }
            if (y == 2){
                return btn5.getId();
            }
        }
        if (x == 2){
            if (y == 0 ){
                return btn6.getId();
            }
            if (y == 1) {
                return btn7.getId();
            }
            if (y == 2){
                return btn8.getId();
            }
        }
        return 1;
    }

    public void setGUIDefault(){
        this.btn0.setText("");
        this.btn1.setText("");
        this.btn2.setText("");
        this.btn3.setText("");
        this.btn4.setText("");
        this.btn5.setText("");
        this.btn6.setText("");
        this.btn7.setText("");
        this.btn8.setText("");
        this.status.setText("You: X\nAI: O\nMINIMAX Algorithm");
    }
}
